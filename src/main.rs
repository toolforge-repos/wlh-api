#[macro_use]
extern crate rocket;

use anyhow::Result;
use mwseaql::sea_query::{Alias, Expr, JoinType, MysqlQueryBuilder, Order, Query};
use mwseaql::{Page as PageTable, Pagelinks, Redirect as RedirectTable};
use mysql_async::{prelude::*, Conn, Pool};
use rocket::fairing::{Fairing, Info, Kind};
use rocket::http::{Header, Status};
use rocket::serde::json::Json;
use rocket::serde::Serialize;
use rocket::{Request, Response, State};
use std::collections::HashMap;
use std::env;

struct Cors;

#[rocket::async_trait]
impl Fairing for Cors {
    fn info(&self) -> Info {
        Info {
            name: "Add CORS headers to responses",
            kind: Kind::Response,
        }
    }

    async fn on_response<'r>(&self, _request: &'r Request<'_>, response: &mut Response<'r>) {
        response.set_header(Header::new("Access-Control-Allow-Origin", "*"));
        response.set_header(Header::new(
            "Access-Control-Allow-Methods",
            "POST, GET, PATCH, OPTIONS",
        ));
        response.set_header(Header::new("Access-Control-Allow-Headers", "*"));
        response.set_header(Header::new("Access-Control-Allow-Credentials", "true"));
    }
}

#[derive(Debug, Serialize)]
#[serde(crate = "rocket::serde")]
struct Page {
    title: String,
    namespace: usize,
    bytes: usize,
    last_touched: usize,
}

struct Indirect {
    page: Page,
    redirect_page_id: usize,
}

#[derive(Debug, Serialize)]
#[serde(crate = "rocket::serde")]
struct Redirect {
    title: String,
    namespace: usize,
    to_section: String,
    pages: Vec<Page>,
    #[serde(skip_serializing)]
    page_id: usize,
}

impl Redirect {
    fn new(page_id: usize, title: String, namespace: usize, to_section: String) -> Self {
        Redirect {
            page_id,
            title,
            namespace,
            to_section,
            pages: Vec::<Page>::new(),
        }
    }
}

#[derive(Debug, Serialize)]
#[serde(crate = "rocket::serde")]
struct LinksToPage {
    pages: Vec<Page>,
    redirects: Vec<Redirect>,

    #[serde(skip_serializing)]
    redirect_index_map: HashMap<usize, usize>,
}

impl LinksToPage {
    fn new() -> Self {
        LinksToPage {
            pages: Default::default(),
            redirects: Default::default(),
            redirect_index_map: Default::default(),
        }
    }
    fn add_pages(&mut self, pages: Vec<Page>) {
        self.pages = pages;
    }
    fn add_redirects(&mut self, mut redirects: Vec<Redirect>) {
        for redirect in redirects.drain(0..) {
            let i = self.redirects.len();
            self.redirect_index_map.insert(redirect.page_id, i);
            self.redirects.push(redirect);
        }
    }
    fn add_indirect_links(&mut self, mut indirects: Vec<Indirect>) {
        for indirect in indirects.drain(0..) {
            if let Some(redirect_index) = self.redirect_index_map.get(&indirect.redirect_page_id) {
                self.redirects[*redirect_index].pages.push(indirect.page);
            } else {
                println!(
                    "Warning: unknown redirect page id {}",
                    indirect.redirect_page_id
                );
            }
        }
    }
}

#[get("/")]
async fn index() -> &'static str {
    r#"{"TODO": "documentation"}"#
}

#[get("/backlinks?<title>&<ns>&<from_ns>")]
async fn get_backlinks(
    pool: &State<Pool>,
    title: &str,
    ns: Option<u32>,
    from_ns: Option<&str>,
) -> Result<Json<LinksToPage>, Status> {
    let to_namespace = ns.unwrap_or(0);
    let mut parsed_from_ns = None;
    if let Some(list) = from_ns {
        if let Ok(parsed) = parse_id_list(list) {
            parsed_from_ns = Some(parsed);
        } else {
            return Err(Status::BadRequest); // was not a list of numbers
        }
    }

    let mut conn = pool.get_conn().await.map_err(|err| {
        println!("{}", err);
        Status::InternalServerError
    })?;
    let mut links_to_page = LinksToPage::new();
    match get_direct_links(
        &mut conn,
        to_namespace,
        title.to_string(),
        parsed_from_ns.clone(),
    )
    .await
    {
        Ok(pages) => links_to_page.add_pages(pages),
        Err(err) => {
            println!("{}", err);
            return Err(Status::InternalServerError);
        }
    }
    match get_redirects(
        &mut conn,
        to_namespace,
        title.to_string(),
        parsed_from_ns.clone(),
    )
    .await
    {
        Ok(redirects) => links_to_page.add_redirects(redirects),
        Err(err) => {
            println!("{}", err);
            return Err(Status::InternalServerError);
        }
    }
    match get_indirect_links(&mut conn, to_namespace, title.to_string(), parsed_from_ns).await {
        Ok(indirects) => links_to_page.add_indirect_links(indirects),
        Err(err) => {
            println!("{}", err);
            return Err(Status::InternalServerError);
        }
    }
    Ok(Json(links_to_page))
}

/// Parse a list of comma-separated numbers into a Vec<u32>, failing if
/// any of the items are not a number.
fn parse_id_list(number_list: &str) -> Result<Vec<u32>> {
    let mut numerals = vec![];
    for maybe_num in number_list.split(',') {
        if let Ok(num) = maybe_num.parse() {
            numerals.push(num);
        } else {
            return Err(anyhow::anyhow!("{} is not a number", maybe_num));
        }
    }
    Ok(numerals)
}

async fn get_direct_links(
    conn: &mut Conn,
    namespace: u32,
    page_name: String,
    from_ns: Option<Vec<u32>>,
) -> Result<Vec<Page>> {
    let query = {
        let mut builder = Query::select();
        builder
            .columns([
                PageTable::Title,
                PageTable::Namespace,
                PageTable::Len,
                PageTable::Touched,
            ])
            .from(PageTable::Table)
            .inner_join(
                Pagelinks::Table,
                Expr::tbl(Pagelinks::Table, Pagelinks::From)
                    .equals(PageTable::Table, PageTable::Id),
            )
            .and_where(Expr::col(Pagelinks::Namespace).eq(namespace))
            .and_where(Expr::col(Pagelinks::Title).eq(page_name))
            .order_by_columns([
                (PageTable::Namespace, Order::Asc),
                (PageTable::Title, Order::Asc),
            ]);
        if let Some(from_ns) = from_ns {
            builder.and_where(Expr::col(Pagelinks::FromNamespace).is_in(from_ns));
        }
        builder.to_string(MysqlQueryBuilder)
    };
    let pages = conn
        .query_map(query, |(title, namespace, bytes, last_touched)| Page {
            title,
            namespace,
            bytes,
            last_touched,
        })
        .await?;
    Ok(pages)
}

async fn get_redirects(
    conn: &mut Conn,
    namespace: u32,
    page_name: String,
    from_ns: Option<Vec<u32>>,
) -> Result<Vec<Redirect>> {
    let query = {
        let mut builder = Query::select();
        builder
            .columns([PageTable::Id, PageTable::Title, PageTable::Namespace])
            .column(RedirectTable::Fragment)
            .from(RedirectTable::Table)
            .inner_join(
                PageTable::Table,
                Expr::tbl(PageTable::Table, PageTable::Id)
                    .equals(RedirectTable::Table, RedirectTable::From),
            )
            .and_where(Expr::col(RedirectTable::Namespace).eq(namespace))
            .and_where(Expr::col(RedirectTable::Title).eq(page_name))
            .order_by_columns([
                (PageTable::Namespace, Order::Asc),
                (PageTable::Title, Order::Asc),
            ]);
        if let Some(from_ns) = from_ns {
            builder.and_where(Expr::col(PageTable::Namespace).is_in(from_ns));
        }
        builder.to_string(MysqlQueryBuilder)
    };
    let pages = conn
        .query_map(query, |(page_id, title, namespace, to_section)| {
            Redirect::new(page_id, title, namespace, to_section)
        })
        .await?;
    Ok(pages)
}

async fn get_indirect_links(
    conn: &mut Conn,
    namespace: u32,
    page_name: String,
    from_ns: Option<Vec<u32>>,
) -> Result<Vec<Indirect>> {
    let query = {
        let mut builder = Query::select();
        let redir_page = Alias::new("redir_page");
        let pl_page = Alias::new("pl_page");
        builder
            .column((pl_page.clone(), PageTable::Title))
            .column(Pagelinks::FromNamespace)
            .column((pl_page.clone(), PageTable::Len))
            .column((pl_page.clone(), PageTable::Touched))
            .column((redir_page.clone(), PageTable::Id))
            .from(RedirectTable::Table)
            .join_as(
                JoinType::InnerJoin,
                PageTable::Table,
                redir_page.clone(),
                Expr::tbl(redir_page.clone(), PageTable::Id)
                    .equals(RedirectTable::Table, RedirectTable::From),
            )
            .inner_join(
                Pagelinks::Table,
                Expr::tbl(Pagelinks::Table, Pagelinks::Namespace)
                    .equals(RedirectTable::Table, RedirectTable::Namespace)
                    .and(
                        Expr::tbl(Pagelinks::Table, Pagelinks::Title)
                            .equals(redir_page, PageTable::Title),
                    ),
            )
            .join_as(
                JoinType::InnerJoin,
                PageTable::Table,
                pl_page.clone(),
                Expr::tbl(Pagelinks::Table, Pagelinks::From).equals(pl_page.clone(), PageTable::Id),
            )
            .and_where(Expr::col(RedirectTable::Namespace).eq(namespace))
            .and_where(Expr::col(RedirectTable::Title).eq(page_name))
            .order_by(Pagelinks::FromNamespace, Order::Asc)
            .order_by((pl_page, PageTable::Title), Order::Asc);
        if let Some(from_ns) = from_ns {
            builder.and_where(Expr::col(Pagelinks::FromNamespace).is_in(from_ns));
        }
        builder.to_string(MysqlQueryBuilder)
    };
    let pages = conn
        .query_map(
            query,
            |(title, namespace, bytes, last_touched, redirect_page_id)| Indirect {
                page: Page {
                    title,
                    namespace,
                    bytes,
                    last_touched,
                },
                redirect_page_id,
            },
        )
        .await?;
    Ok(pages)
}

fn db_conn() -> Result<mysql_async::Pool> {
    let port = env::var("MYSQL_PORT");
    let user = env::var("MYSQL_USER");
    let pass = env::var("MYSQL_PASS");
    let db_url = if port.is_ok() && user.is_ok() && pass.is_ok() {
        format!(
            "mysql://{}:{}@localhost:{}/enwiki_p",
            user.unwrap(),
            pass.unwrap(),
            port.unwrap()
        )
    } else {
        toolforge::connection_info!("enwiki_p", WEB)?.to_string()
    };
    Ok(mysql_async::Pool::new(db_url.as_str()))
}

#[launch]
fn rocket() -> _ {
    rocket::build()
        .manage(db_conn().expect("failed to load db config"))
        .mount("/", routes![index, get_backlinks])
        .attach(Cors)
}
